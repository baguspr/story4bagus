from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('contact/', views.contact, name='contact'),
    path('gallery/', views.gallery, name='gallery'),
    path('friendform/', views.friendform, name='friendform'),
    path('friendlist/', views.friendlist, name='friendlist'),
    path('thanks/', views.thanks, name='thanks'),
]