from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import DataForm
from .models import Data

# Create your views here.

def home(request):
    return render(request, 'homepage/home.html')

def about(request):
    return render(request, 'homepage/about.html')

def contact(request):
    return render(request, 'homepage/contact.html')

def gallery(request):
    return render(request, 'homepage/gallery.html')

def friendform(request):
    if request.method == 'POST':
        form = DataForm(request.POST)
        if form.is_valid():
            data_item = form.save(commit=False)
            data_item.save()
            return render(request, 'homepage/thanks.html')
    else:
        form = DataForm()
        return render(request, 'homepage/friendform.html', {'form' : form})

def friendlist(request):
    allfriends = Data.objects.all()
    return render(request, 'homepage/friendlist.html', {'allfriends':allfriends})

def thanks(request):
    return render(request, 'homepage/thanks.html')