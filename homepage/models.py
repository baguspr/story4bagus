from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.

class Year(models.Model):
    year = models.CharField(max_length=6,primary_key=True)

    def __str__(self):
        return '%s' % self.year

class Data(models.Model):
    name = models.CharField(primary_key=True, max_length=30)
    hobby = models.CharField(max_length=30)
    your_year = models.ForeignKey(Year, on_delete=models.CASCADE)
    fnb = models.CharField(max_length=30)

    def __str__(self):
        return '%s' % self.name